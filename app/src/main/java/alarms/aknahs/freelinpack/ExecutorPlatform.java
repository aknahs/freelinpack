package alarms.aknahs.freelinpack;

/**
 * Created by aknahs on 22/06/15.
 */

import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ExecutorPlatform {
    Executor mExecutor = Executors.newFixedThreadPool(10);
    private volatile static boolean mMultithread = false;

    private volatile static int NTHREADS = 4;
    private volatile static int NCYCLES = 20;

    // private double[][][] mMultiResults; // = new double[10][10][6];
    private static volatile Integer mMultiIndex = 0;
    private static volatile Integer mCycle = 0;

    private static double[] mTotalResults = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    private static double[] mCycleResult = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    private static double mMinTime = Double.MAX_VALUE;
    private static double mMaxTime = 0;

    public ExecutorPlatform mThis = this;

    MainActivity activity;

    public ExecutorPlatform(MainActivity a) {
        activity = a;
    }

    public void singleBenchmark(int ncycles, int nthreads, int nmatrix) {

        NCYCLES = ncycles;
        NTHREADS = nthreads;

        Log.v("Linpack", "NCYCLES = " + NCYCLES + "; NTHREADS = " + NTHREADS
                + " MMATRIX = " + nmatrix);

        mMultithread = false;
        mMultiIndex = 0;
        mCycle = 0;

        mMinTime = Double.MAX_VALUE;
        mMaxTime = 0;

        for (int i = 0; i < 6; i++) {
            mTotalResults[i] = 0;
            mCycleResult[i] = 0;
        }

        Linpack l = new Linpack();
        l.setPsize(nmatrix);
        l.execute(mThis);
    }

    public void threadBenchmark(int ncycles, int nthreads, int nmatrix) {

        NCYCLES = ncycles;
        NTHREADS = nthreads;

        Log.v("Linpack", "NCYCLES = " + NCYCLES + "; NTHREADS = " + NTHREADS
                + " MMATRIX = " + nmatrix);

        mMultithread = true;
        mMultiIndex = 0;
        mCycle = 0;

        mMinTime = Double.MAX_VALUE;
        mMaxTime = 0;

        for (int i = 0; i < 6; i++) {
            mTotalResults[i] = 0;
            mCycleResult[i] = 0;
        }

        ArrayList<Linpack> linpacks = new ArrayList<Linpack>();

        for (int i = 0; i < NTHREADS; i++) {
            Linpack l = new Linpack();
            l.setPsize(nmatrix);
            linpacks.add(l);
        }

        for (Linpack l : linpacks) {
            l.executeOnExecutor(mExecutor, mThis);
        }
    }

    public void notifyBenchmarkCompletion(double[] results) {
        if (mMultithread) {
            int index;
            int cycle;

            synchronized (mMultiIndex) {
                index = mMultiIndex;
                cycle = mCycle;

                mMultiIndex++;

                if (mMultiIndex == NTHREADS) {
                    mCycle++;
                    mMultiIndex = 0;
                }
            }

            Log.v("Linpack", "Cycle " + cycle + "[" + NCYCLES + "]"
                    + ", Thread " + index + "[" + NTHREADS + "]" + " : "
                    + Linpack.textResults(results));

            // if (cycle == mCycle) {

            // Set minTime of cycle!
            mMinTime = (results[6] < mMinTime) ? results[6] : mMinTime;

			/* This will be calculated in the end of the cycle */
            mCycleResult[0] = 0;

			/* Sum of all MFLOP */
            mCycleResult[4] += results[4];

			/*
			 * total time. Because we set the min time to the first thread min
			 * time, the total time has to be recomputed
			 */
            double second = results[5] + results[6]; // second = total +
            // time

            if (second > mMaxTime)
                mMaxTime = second;

            // double total = second - minTime;

			/* Keep the maximum total time */
            // cycleResult[5] = (total > cycleResult[5]) ? total
            // : cycleResult[5];

			/* Keep max norm res */
            mCycleResult[1] = (results[1] > mCycleResult[1]) ? results[1]
                    : mCycleResult[1];

			/* Keep worst precision error */
            mCycleResult[3] = (results[3] > mCycleResult[3]) ? results[3]
                    : mCycleResult[3];

            // } else

            if (cycle < mCycle) {

                double total = mMaxTime - mMinTime;

                mCycleResult[5] = total;

				/*
				 * Calculate the total MFLOP/s by dividing the sum of all thread
				 * MFLOPs by the max time
				 */
                mCycleResult[0] = mCycleResult[4] / (1.0e6 * mCycleResult[5]);
                mCycleResult[0] += 0.0005; // for rounding
                mCycleResult[0] = (int) (mCycleResult[0] * 1000);
                mCycleResult[0] /= 1000;

                mCycleResult[2] = mCycleResult[5];
                mCycleResult[2] += 0.005; // for rounding
                mCycleResult[2] = (int) (mCycleResult[2] * 100);
                mCycleResult[2] /= 100;

                Log.v("Linpack",
                        "Cycle result : " + Linpack.textResults(mCycleResult));

                // Store only best cycle
                if (mCycleResult[0] > mTotalResults[0])
                    mTotalResults = mCycleResult;

                mMinTime = Double.MAX_VALUE;
                mCycleResult = new double[7];
                for (int i = 0; i < 7; i++) {
                    mCycleResult[i] = 0;
                }
                mMaxTime = 0;

            }

            // mMultiResults[cycle][index] = results;

            if (mCycle == NCYCLES) {

                Log.v("Linpack", "Concluded ALL cycles!");

				/*
				 * Once all cycles have been completed and all threads responded
				 * present results
				 */

                if (activity.mBestResult < mTotalResults[0]) {
                    SharedPreferences settings = activity.getSharedPreferences(
                            "Linpack", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("best", "" + mTotalResults[0]);
                    // Commit the edits!
                    editor.commit();
                    activity.mBestResult = mTotalResults[0];
                }

                Linpack.resetTime();

                Log.v("Linpack",
                        "Best cycle : " + Linpack.textResults(mTotalResults));

                activity.setBenchmarkResult(Linpack.textResults(mTotalResults));
                activity.setActiveButton();

            } else {

                if (cycle < mCycle) { // Changed cycle

					/* Run again the benchmark up to NCYCLE times */
                    activity.setBenchmarkResult("Cycle :" + mCycle + "...");

                    Linpack.resetTime();

                    // for (int i = 0; i < NTHREADS; i++) {
                    // //new Linpack().execute((MainActivity) mThis);
                    // new Linpack().executeOnExecutor(mExecutor,(MainActivity)
                    // mThis);
                    // }

                    ArrayList<Linpack> linpacks = new ArrayList<Linpack>();

                    for (int i = 0; i < NTHREADS; i++) {
                        Linpack l = new Linpack();
                        linpacks.add(l);
                    }

                    for (Linpack l : linpacks) {
                        l.executeOnExecutor(mExecutor, this);
                    }
                }
            }

        } else {

            synchronized (mMultiIndex) {
                mCycle++;
            }

            Log.v("Linpack",
                    "Cycle " + mCycle + "[" + NCYCLES + "]"
                            + Linpack.textResults(results));

            if (results[0] > mTotalResults[0]) {
                mTotalResults = results;
            }

            if (mCycle >= NCYCLES) {

				/* Set result */
                if (activity.mBestResult < mTotalResults[0]) {
                    SharedPreferences settings = activity.getSharedPreferences(
                            "Linpack", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("best", "" + mTotalResults[0]);
                    // Commit the edits!
                    editor.commit();
                    activity.mBestResult = mTotalResults[0];
                }

                Linpack.resetTime();

                activity.setBenchmarkResult(Linpack.textResults(mTotalResults));
                activity.setActiveButton();

            } else {

                mCycleResult = new double[7];
                for (int i = 0; i < 7; i++) {
                    mCycleResult[i] = 0;
                }
                mMaxTime = 0;
                Linpack.resetTime();

				/* Run again the benchmark up to NCYCLE times */
                activity.setBenchmarkResult("Cycle :" + mCycle + "...");

                new Linpack().execute(mThis);
            }

        }

    }

}
