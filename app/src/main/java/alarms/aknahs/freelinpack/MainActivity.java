package alarms.aknahs.freelinpack;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    Button mBenchmarkButton;
    Button mMultiButton;
    TextView mResult;

    NumberPicker mThreads;
    NumberPicker mCycles;
    NumberPicker mMatrix;

    ExecutorPlatform executorPlatform = new ExecutorPlatform(this);

    double mBestResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);

        // Restore preferences
        SharedPreferences settings = getSharedPreferences("Linpack", 0);
        mBestResult = new Double(settings.getString("best", "0.0"));

        mBenchmarkButton = (Button) findViewById(R.id.benchmark);
        mMultiButton = (Button) findViewById(R.id.button1);
        mResult = (TextView) findViewById(R.id.textView1);
        mThreads = (NumberPicker) findViewById(R.id.numberPicker1);
        mCycles = (NumberPicker) findViewById(R.id.numberPicker2);
        mMatrix = (NumberPicker) findViewById(R.id.numberPicker3);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mThreads.setMinValue(1);
        mThreads.setMaxValue(10);
        mThreads.setValue(4);
        mThreads.setWrapSelectorWheel(false);

        mCycles.setMinValue(1);
        mCycles.setMaxValue(20);
        mCycles.setValue(2);
        mCycles.setWrapSelectorWheel(false);
        mCycles.setValue(20);


        mMatrix.setMinValue(1);
        mMatrix.setMaxValue(10);
        mMatrix.setWrapSelectorWheel(false);

        int minValue = 100;
        int maxValue = 1000;
        int step = 100;

        String[] valueSet = new String[1 + (maxValue - minValue) / step];

        for (int i = 0; i < 1 + (maxValue - minValue) / step; i++) {
            valueSet[i] = String.valueOf(minValue + step * i);
        }

        mMatrix.setDisplayedValues(valueSet);

        mMatrix.setWrapSelectorWheel(false);

        mMatrix.setValue(5);

        mResult.setText("Best Mflops/s: " + mBestResult + "\n");

        mBenchmarkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setInactiveButton();

                int ncycles = mCycles.getValue();
                int nthreads = mThreads.getValue();
                int nmatrix = mMatrix.getValue() * 100;

                executorPlatform.singleBenchmark(ncycles, nthreads, nmatrix);

                setBenchmarkResult("NCYCLES = " + ncycles + "; NTHREADS = "
                        + nthreads + " MMATRIX = " + nmatrix);

            }
        });

        mMultiButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setInactiveButton();

                int ncycles = mCycles.getValue();
                int nthreads = mThreads.getValue();
                int nmatrix = mMatrix.getValue() * 100;

                executorPlatform.threadBenchmark(ncycles, nthreads, nmatrix);

                setBenchmarkResult("NCYCLES = " + ncycles + "; NTHREADS = "
                        + nthreads + " MMATRIX = " + nmatrix);

            }
        });

        //Only for automated testing
        //automateStart();
    }

    public void automateStart(){
        setInactiveButton();

        int ncycles = mCycles.getValue();
        int nthreads = mThreads.getValue();
        int nmatrix = mMatrix.getValue() * 100;

        executorPlatform.threadBenchmark(ncycles, nthreads, nmatrix);

        setBenchmarkResult("NCYCLES = " + ncycles + "; NTHREADS = "
                + nthreads + " MMATRIX = " + nmatrix);
    }

    public void setBenchmarkResult(String txt) {
        mResult.setText("Best Mflops/s: " + mBestResult + "\n" + txt);
        Log.v("FINALRES", txt);
    }

    public void setActiveButton() {
        mBenchmarkButton.setEnabled(true);
        mMultiButton.setEnabled(true);
        mThreads.setEnabled(true);
        mCycles.setEnabled(true);
        mMatrix.setEnabled(true);
        // MainActivity.mThis.setProgressBarIndeterminateVisibility(false);
    }

    public void setInactiveButton() {
        mBenchmarkButton.setEnabled(false);
        mMultiButton.setEnabled(false);
        mThreads.setEnabled(false);
        mCycles.setEnabled(false);
        mMatrix.setEnabled(false);
        // MainActivity.mThis.setProgressBarIndeterminateVisibility(true);
    }
}
